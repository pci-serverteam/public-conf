upstream base_domain-odoo {
    server 127.0.0.1:8069;
}

upstream base_domain-long {
    server 127.0.0.1:8072;
}

upstream base_domain-pcictl {
    server 127.0.0.1:5555;
}

server {
	listen 80;
	server_name base_domain;

    location ~* /.well-known { 
        default_type "text/plain"; 
        root /var/www/html;
    }

    location / {

        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-Forwarded-Host $host;

        proxy_pass         http://base_domain-odoo;

        proxy_connect_timeout 120s;
        proxy_send_timeout   3600;
        proxy_read_timeout   3600;

    }

    # cache some static data in memory for 60mins
    location ~* /web/static/ {
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-Forwarded-Host $host;
        proxy_set_header X-NginX-proxy true;

        proxy_cache_valid 200 60m;
        proxy_buffering on;
        expires 864000;
        add_header Cache-Control "public, max-age=4h";
        proxy_pass http://base_domain-odoo;

        proxy_hide_header Set-Cookie;
        }

    location ~* /web/image/ {
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-Forwarded-Host $host;
        proxy_set_header X-NginX-proxy true;

        proxy_cache_valid 200 60m;
        proxy_buffering on;
        expires 864000;
        add_header Cache-Control "public, max-age=4h";
        proxy_pass http://base_domain-odoo;

        proxy_hide_header Set-Cookie;
        }

    location /longpolling {

        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-Forwarded-Host $host;

        proxy_pass         http://base_domain-long;

        proxy_connect_timeout 120s;
        proxy_send_timeout   3600;
        proxy_read_timeout   3600;
    }

    location /websocket {

        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-Forwarded-Host $host;

        proxy_pass         http://base_domain-long;

        proxy_connect_timeout 120s;
        proxy_send_timeout   3600;
        proxy_read_timeout   86400;

        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "upgrade";

    }
    location /pcictl {

        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-Forwarded-Host $host;

        proxy_pass         http://base_domain-pcictl;

        proxy_connect_timeout 120s;
        proxy_send_timeout   3600;
        proxy_read_timeout   3600;

    }

    # common gzip
    gzip_types text/css text/less text/plain text/xml application/xml application/json application/javascript;
    gzip on;

    add_header X-XSS-Protection "1; mode=block" always;
    add_header X-Content-Type-Options nosniff always;
    add_header Content-Security-Policy "default-src *; font-src *;img-src * data: blob:; script-src * 'unsafe-inline' 'unsafe-eval'; style-src * 'unsafe-inline'; frame-src *";
    add_header X-Frame-Options "SAMEORIGIN";
    add_header Referrer-Policy "strict-origin-when-cross-origin";
    add_header Permissions-Policy "geolocation=(self),midi=(self),sync-xhr=(slef),microphone=(self),camera=(self),magnetometer=(self),gyroscope=(self),fullscreen=(self),payment=(self)";
    client_max_body_size 2000M;
    proxy_max_temp_file_size 5000M; 
    proxy_cookie_path / "/; secure";

}